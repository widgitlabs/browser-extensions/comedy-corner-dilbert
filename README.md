# Comedy Corner - Dilbert

[![Chrome Web Store](https://img.shields.io/chrome-web-store/v/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb) [![Chrome Web Store](https://img.shields.io/chrome-web-store/users/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb) [![Chrome Web Store](https://img.shields.io/chrome-web-store/rating/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb)  [![License](https://img.shields.io/badge/license-GPL--2.0%2B-red.svg)](https://github.com/evertiro/comedy-corner-dilbert/blob/master/license.txt)

## Welcome to our GitLab Repository

Comedy Corner presents your daily dose of Dilbert.

### What is Dilbert

Dilbert is an American comic strip written and illustrated by Scott Adams, first published on April 16, 1989. The strip is known for its satirical office humor about a white-collar, micromanaged office featuring engineer Dilbert as the title character.

### What is this extension

This extension adds Dilbert to your toolbar!

* Allows navigation between previous and next comic strips.
* Allows selection of specific comics through a simple date picker.
* Allows display of a random comic strip.
* Comics link to their respective pages on dilbert.com.
* Remembers the last comic you viewed!

## Installation

[![Chrome Web Store](https://img.shields.io/chrome-web-store/v/lfddnkicgngldglbjdiammicccpangcb.svg)](https://chrome.google.com/webstore/detail/comedy-corner-dilbert/lfddnkicgngldglbjdiammicccpangcb)

## Bugs

If you find an issue, let us know [here](https://gitlab.com/evertiro/comedy-corner-dilbert/issues)!